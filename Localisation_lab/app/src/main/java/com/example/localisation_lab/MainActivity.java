package com.example.localisation_lab;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    TextView txt_latlng;
    TextView txt_location;
    Button btn_findme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt_latlng = findViewById(R.id.latlng);
        txt_location = findViewById(R.id.location_txt);

        //check permission first
        checkPermission();
    }


    public void btn_findme(View view){

        //txt_latlng.setText("BLABLA");

        SingleShotLocationProvider.requestSingleUpdate(this, new SingleShotLocationProvider.LocationCallBack() {
            @Override
            public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                txt_latlng.setText("LAT:" + String.valueOf(location.latitude) + "; LNG: "+ String.valueOf(location.longitude));

                Geocoder geocoder = new Geocoder(MainActivity.this);
                try {
                    //Toast.makeText(MainActivity.this, geocoder.getFromLocation(location.latitude, location.longitude, 1).toString(), Toast.LENGTH_LONG).show();
                   txt_location.setText(geocoder.getFromLocation(location.latitude, location.longitude, 1).get(0).getAddressLine(0));
                }catch (IOException ioex){
                    Log.d("TAG EX", ioex.getMessage());
                }

            }
        });


    }


    public void checkPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(this, "You are allowed to use Android Location services", Toast.LENGTH_LONG).show();

            } else {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.INTERNET}, 1);
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {

        } else {
            //checkPermission();
            Toast.makeText(this, "PERMISSION DENIED", Toast.LENGTH_LONG).show();
        }
    }
}